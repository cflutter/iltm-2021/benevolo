import 'package:prueba_listas/modulo__list_ii.dart';
import 'package:test/test.dart';

void main() {
  group('Para tiene77', () {
    test('[1, 7, 7] se espera true', () {
      final resultado = tiene77([1, 7, 7]);
      expect(resultado, true);
    });
    test('[1, 7, 1, 7] se espera true', () {
      final resultado = tiene77([1, 7, 1, 7]);
      expect(resultado, true);
    });
    test('[1, 7, 1, 1, 7] se espera false', () {
      final resultado = tiene77([1, 7, 1, 1, 7]);
      expect(resultado, false);
    });
    test('[7, 7, 1, 1, 7] se espera true', () {
      final resultado = tiene77([7, 7, 1, 1, 7]);
      expect(resultado, true);
    });
    test('[2, 7, 2, 2, 7, 2] se espera false', () {
      final resultado = tiene77([2, 7, 2, 2, 7, 2]);
      expect(resultado, false);
    });
    test('[2, 7, 2, 2, 7, 7] se espera true', () {
      final resultado = tiene77([2, 7, 2, 2, 7, 7]);
      expect(resultado, true);
    });
    test('[7, 2, 7, 2, 2, 7] se espera true', () {
      final resultado = tiene77([7, 2, 7, 2, 2, 7]);
      expect(resultado, true);
    });
    test('[7, 2, 6, 2, 2, 7] se espera false', () {
      final resultado = tiene77([7, 2, 6, 2, 2, 7]);
      expect(resultado, false);
    });
    test('[7, 7, 7] se espera true', () {
      final resultado = tiene77([7, 7, 7]);
      expect(resultado, true);
    });
    test('[7, 1, 7] se espera true', () {
      final resultado = tiene77([7, 1, 7]);
      expect(resultado, true);
    });
    test('[7, 1, 1] se espera false', () {
      final resultado = tiene77([7, 1, 1]);
      expect(resultado, false);
    });

    test('[1,2] se espera false', () {
      final resultado = tiene77([1, 2]);
      expect(resultado, false);
    });
    test('[1,7] se espera false', () {
      final resultado = tiene77([1, 7]);
      expect(resultado, false);
    });
    test('[7] se espera false', () {
      final resultado = tiene77([7]);
      expect(resultado, false);
    });
    test('[] se espera false', () {
      final resultado = tiene77([]);
      expect(resultado, false);
    });
  });

  group('para esBalanceable', () {
    test('con [1, 1, 1, 2, 1] es true', () {
      final resultado = esBalanceable([1, 1, 1, 2, 1]);
      expect(resultado, true);
    });
    test('con [2, 1, 1, 2, 1] es false', () {
      final resultado = esBalanceable([2, 1, 1, 2, 1]);
      expect(resultado, false);
    });
    test('con [10, 10] es true', () {
      final resultado = esBalanceable([10, 10]);
      expect(resultado, true);
    });
    test('con [10, 0, 1, -1, 10] es true', () {
      final resultado = esBalanceable([10, 0, 1, -1, 10]);
      expect(resultado, true);
    });
    test('con [1, 1, 1, 1, 4] es true', () {
      final resultado = esBalanceable([1, 1, 1, 1, 4]);
      expect(resultado, true);
    });
    test('con [2, 1, 1, 1, 4] es false', () {
      final resultado = esBalanceable([2, 1, 1, 1, 4]);
      expect(resultado, false);
    });
    test('con [2, 3, 4, 1, 2] es false', () {
      final resultado = esBalanceable([2, 3, 4, 1, 2]);
      expect(resultado, false);
    });
    test('con [1, 2, 3, 1, 0, 2, 3] es true', () {
      final resultado = esBalanceable([1, 2, 3, 1, 0, 2, 3]);
      expect(resultado, true);
    });
    test('con [1, 2, 3, 1, 0, 1, 3] es false', () {
      final resultado = esBalanceable([1, 2, 3, 1, 0, 1, 3]);
      expect(resultado, false);
    });
    test('con [1] es false', () {
      final resultado = esBalanceable([1]);
      expect(resultado, false);
    });
    test('con [1, 1, 1, 2, 1] es true', () {
      final resultado = esBalanceable([1, 1, 1, 2, 1]);
      expect(resultado, true);
    });
  });
}
