import 'package:prueba_listas/modulo__list_i.dart';
import 'package:test/test.dart';
import 'package:collection/collection.dart';

void main() {
  group('Sin 10', () {
    test('Con [1,10,10,2] se espera [1,2,0,0]', () {
      final resultado = sin10([1, 10, 10, 2]);
      final comparacion = ListEquality().equals(resultado, [1, 2, 0, 0]);
      expect(true, comparacion);
    });
    test('Con [10,2,10] se espera [2,0,0]', () {
      final resultado = sin10([10, 2, 10]);
      final comparacion = ListEquality().equals(resultado, [2, 0, 0]);
      expect(true, comparacion);
    });
    test('Con [1,99,10] se espera [1,99,0]', () {
      final resultado = sin10([1, 99, 10]);
      final comparacion = ListEquality().equals(resultado, [1, 99, 0]);
      expect(true, comparacion);
    });
    test('Con [10,13,10,14] se espera [13,14,0,0]', () {
      final resultado = sin10([10, 13, 10, 14]);
      final comparacion = ListEquality().equals(resultado, [13, 14, 0, 0]);
      expect(true, comparacion);
    });

    test('Con [10,13,10,14,10] se espera [13,14,0,0,0]', () {
      final resultado = sin10([10, 13, 10, 14, 10]);
      final comparacion = ListEquality().equals(resultado, [13, 14, 0, 0, 0]);
      expect(true, comparacion);
    });

    test('Con [10,10,3] se espera [3,0,0]', () {
      final resultado = sin10([10, 10, 3]);
      final comparacion = ListEquality().equals(resultado, [3, 0, 0]);
      expect(true, comparacion);
    });

    test('Con [1] se espera [1]', () {
      final resultado = sin10([1]);
      final comparacion = ListEquality().equals(resultado, [1]);
      expect(true, comparacion);
    });

    test('Con [13,1] se espera [13,1]', () {
      final resultado = sin10([13, 1]);
      final comparacion = ListEquality().equals(resultado, [13, 1]);
      expect(true, comparacion);
    });

    test('Con [10] se espera [0]', () {
      final resultado = sin10([10]);
      final comparacion = ListEquality().equals(resultado, [0]);
      expect(true, comparacion);
    });

    test('Con [] se espera []', () {
      final resultado = sin10([]);
      final comparacion = ListEquality().equals(resultado, []);
      expect(true, comparacion);
    });
  });

  group('Para fizzBuzz', () {
    test('dado 1,6 se espera  ["1", "2", "Fizz", "4", "Buzz"]', () {
      final resultado = fizzBuzz(1, 6);
      final comparacion =
          ListEquality().equals(resultado, ['1', '2', 'Fizz', '4', 'Buzz']);
      expect(true, comparacion);
    });
    test('dado 1,8 se espera ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7"]', () {
      final resultado = fizzBuzz(1, 8);
      final comparacion = ListEquality()
          .equals(resultado, ['1', '2', 'Fizz', '4', 'Buzz', 'Fizz', '7']);
      expect(true, comparacion);
    });

    test(
        'dado 1,11 se espera  ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz"]',
        () {
      final resultado = fizzBuzz(1, 11);
      final comparacion = ListEquality().equals(resultado,
          ['1', '2', 'Fizz', '4', 'Buzz', 'Fizz', '7', '8', 'Fizz', 'Buzz']);
      expect(true, comparacion);
    });

    test(
        'dado 1,16 se espera ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"] ',
        () {
      final resultado = fizzBuzz(1, 16);
      final comparacion = ListEquality().equals(resultado, [
        '1',
        '2',
        'Fizz',
        '4',
        'Buzz',
        'Fizz',
        '7',
        '8',
        'Fizz',
        'Buzz',
        '11',
        'Fizz',
        '13',
        '14',
        'FizzBuzz'
      ]);
      expect(true, comparacion);
    });

    test('dado 1,4 se espera ["1", "2", "Fizz"] ', () {
      final resultado = fizzBuzz(1, 4);
      final comparacion = ListEquality().equals(resultado, ['1', '2', 'Fizz']);
      expect(true, comparacion);
    });
    test('dado 1,2 se espera ["1"] ', () {
      final resultado = fizzBuzz(1, 2);
      final comparacion = ListEquality().equals(resultado, ['1']);
      expect(true, comparacion);
    });

    test('dado 50,56  se espera  ["Buzz", "Fizz", "52", "53", "Fizz", "Buzz"]',
        () {
      final resultado = fizzBuzz(50, 56);
      final comparacion = ListEquality()
          .equals(resultado, ['Buzz', 'Fizz', '52', '53', 'Fizz', 'Buzz']);
      expect(true, comparacion);
    });

    test('dado 15,17  se espera ["FizzBuzz", "16"] ', () {
      final resultado = fizzBuzz(15, 17);
      final comparacion = ListEquality().equals(resultado, ['FizzBuzz', '16']);
      expect(true, comparacion);
    });

    test('dado 30,36  se espera  ["FizzBuzz", "31", "32", "Fizz", "34", "Buzz"]', () {
      final resultado = fizzBuzz(30, 36);
      final comparacion = ListEquality().equals(resultado, ['FizzBuzz', '31', '32', 'Fizz', '34', 'Buzz']);
      expect(true, comparacion);
    });

    test('dado 1000,1006  se espera  ["Buzz", "1001", "Fizz", "1003", "1004", "FizzBuzz"]', () {
      final resultado = fizzBuzz(1000, 1006);
      final comparacion = ListEquality().equals(resultado, ['Buzz', '1001', 'Fizz', '1003', '1004', 'FizzBuzz']);
      expect(true, comparacion);
    });

    test('dado 99,102  se espera  ["Fizz", "Buzz", "101"]', () {
      final resultado = fizzBuzz(99, 102);
      final comparacion = ListEquality().equals(resultado, ['Fizz', 'Buzz', '101']);
      expect(true, comparacion);
    });

    test('dado 14,20  se espera  ["14", "FizzBuzz", "16", "17", "Fizz", "19"]', () {
      final resultado = fizzBuzz(14, 20);
      final comparacion = ListEquality().equals(resultado, ['14', 'FizzBuzz', '16', '17', 'Fizz', '19']);
      expect(true, comparacion);
    });


  });
}
