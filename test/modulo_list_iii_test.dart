import 'package:prueba_listas/modulo_list_iii.dart';
import 'package:test/test.dart';

void main() {
  group('Para esContenido', () {
    test('si ([1, 2, 4, 6], [2, 4]) → true', () {
      final resultado = esContenido([1, 2, 4, 6], [2, 4]);
      expect(resultado, true);
    });
    test('si ([1, 2, 4, 6], [2, 3, 4]) → false', () {
      final resultado = esContenido([1, 2, 4, 6], [2, 3, 4]);
      expect(resultado, false);
    });
    test('si ([1, 2, 4, 4, 6], [2, 4]) → true', () {
      final resultado = esContenido([1, 2, 4, 4, 6], [2, 4]);
      expect(resultado, true);
    });
    test('si ([2, 2, 4, 4, 6, 6], [2, 4]) → true', () {
      final resultado = esContenido([2, 2, 4, 4, 6, 6], [2, 4]);
      expect(resultado, true);
    });

    test('si ([2, 2, 2, 2, 2], [2, 2]) → true', () {
      final resultado = esContenido([2, 2, 2, 2, 2], [2, 2]);
      expect(resultado, true);
    });
    test('si ([2, 2, 2, 2, 2], [2, 4]) → false', () {
      final resultado = esContenido([2, 2, 2, 2, 2], [2, 4]);
      expect(resultado, false);
    });
    test('si ([2, 2, 2, 2, 4], [2, 4]) → true', () {
      final resultado = esContenido([2, 2, 2, 2, 4], [2, 4]);
      expect(resultado, true);
    });
    test('si ([1, 2, 3], [2]) → true', () {
      final resultado = esContenido([1, 2, 3], [2]);
      expect(resultado, true);
    });
    test('si ([1, 2, 3], [-1]) → false', () {
      final resultado = esContenido([1, 2, 3], [-1]);
      expect(resultado, false);
    });
    test('si ([1, 2, 3], []) → true', () {
      final resultado = esContenido([1, 2, 3], []);
      expect(resultado, true);
    });
    test('si ([-1, 0, 3, 3, 3, 10, 12], [-1, 0, 3, 12]) → true', () {
      final resultado = esContenido([-1, 0, 3, 3, 3, 10, 12], [-1, 0, 3, 12]);
      expect(resultado, true);
    });
    test('si ([-1, 0, 3, 3, 3, 10, 12], [0, 3, 12, 14]) → false', () {
      final resultado = esContenido([-1, 0, 3, 3, 3, 10, 12], [0, 3, 12, 14]);
      expect(resultado, false);
    });
    test('si ([-1, 0, 3, 3, 3, 10, 12], [-1, 10, 11]) → false', () {
      final resultado = esContenido([-1, 0, 3, 3, 3, 10, 12], [-1, 10, 11]);
      expect(resultado, false);
    });
  });

  group('Para cuentaBonches', () {
    test('con [1, 2, 2, 3, 4, 4] se espera 2', () {
      final resultado = cuentaBonches([1, 2, 2, 3, 4, 4]);
      expect(resultado, 2);
    });
  });
  test('con [1, 1, 2, 1, 1]  se espera 2', () {
      final resultado = cuentaBonches([1, 1, 2, 1, 1]);
      expect(resultado, 2);
    });
  test('con [1, 1, 1, 1, 1]  se espera 1', () {
      final resultado = cuentaBonches([1, 1, 1, 1, 1]);
      expect(resultado, 1);
    });
    test('con [1, 2, 3]  se espera 0', () {
      final resultado = cuentaBonches([1, 2, 3,]);
      expect(resultado, 0);
    });
    test('con [2, 2, 1, 1, 1, 2, 1, 1, 2, 2]  se espera 4', () {
      final resultado = cuentaBonches([2, 2, 1, 1, 1, 2, 1, 1, 2, 2]);
      expect(resultado, 4);
    });
    test('con [0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]  se espera 4', () {
      final resultado = cuentaBonches([0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]);
      expect(resultado, 4);
    });

    test('con [0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]  se espera 5', () {
      final resultado = cuentaBonches([0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]);
      expect(resultado, 5);
    });
    test('con [0, 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]  se espera 5', () {
      final resultado = cuentaBonches([0, 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]);
      expect(resultado, 5);
    });
    test('con []  se espera 0', () {
      final resultado = cuentaBonches([]);
      expect(resultado, 0);
    });
}
